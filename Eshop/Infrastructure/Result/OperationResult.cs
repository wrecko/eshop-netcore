﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Infrastructure
{
    public class OperationResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Model { get; set; }

        public static OperationResult IsSuccess(string msg = "") => new OperationResult() { Success = true, Message = msg };
        public static OperationResult IsSuccess(object model) => new OperationResult() { Success = true, Model = model };
        public static OperationResult IsFailure(string msg = "") => new OperationResult() { Success = false, Message = msg };
    }
}
