﻿using Eshop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Infrastructure
{
    [Serializable]
    public class UserSessionModel
    {
        public string SessionId;

        public string UserName { get; set; }
        public UserRoleEnum Role { get; set; }

        public UserSessionModel(string username, UserRoleEnum role, string sessionId)
        {
            UserName = username;
            Role = role;
            SessionId = sessionId;
        }
        
    }
}
