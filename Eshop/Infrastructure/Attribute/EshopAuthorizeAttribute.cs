﻿using Eshop.Core;
using Eshop.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class EshopAuthorizeAttribute : ActionFilterAttribute
    {
        private UserRoleEnum _userRole;

        public EshopAuthorizeAttribute(UserRoleEnum userRole = UserRoleEnum.USER)
        {
            this._userRole = userRole;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var user = context.HttpContext.Session.GetObjectFromJson<UserSessionModel>("USER");

            if (user == null || user.Role != _userRole || user.SessionId != context.HttpContext.Session.Id)
            {
                context.Result = new RedirectResult("/login");
                return;
            }
               
            
        }
    }
}
