﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Models;

namespace Eshop.Infrastructure.Orchestrator
{
    public interface IAuthOrchestrator
    {
        OperationResult ValidateLogin(LoginModel model);
    }
}
