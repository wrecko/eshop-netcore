﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Core.Database.Entity;
using Eshop.Models.Admin.Product;
using Microsoft.AspNetCore.Hosting;

namespace Eshop.Infrastructure.Orchestrator
{
    public interface IProductOrchestrator
    {
        IList<ProductViewModel> GetProducts();        
        ProductEditCreateModel GetDataForProductCreate();
        OperationResult CreateProduct(ProductEditCreateModel model);
        IList<ProductOptionRelationModel> GetProductOptions(int typeId = 0);
        ProductOptionViewModel GetOptionsViewModel();
        OperationResult CreateProductOptionType(ProductOptionView model);
        OperationResult DeleteProductOptionType(int id);
        OperationResult EditProductOptionType(ProductOptionView model);
        ProductEditCreateModel GetProduct(int id);
        OperationResult UpdateProduct(ProductEditCreateModel model, IHostingEnvironment env);
        OperationResult DeleteProduct(int id);
        OperationResult CreateProductOption(ProductOptionView model);
        OperationResult EditProductOption(ProductOptionView model);
        OperationResult DeleteProductOption(int id);
        OperationResult CreateProductOptionRelation(ProductRelationView model);
        OperationResult DeleteProductOptionRelation(int id);
    }
}
