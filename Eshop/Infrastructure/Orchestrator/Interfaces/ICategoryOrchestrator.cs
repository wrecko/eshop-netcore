﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Core.Database;
using Eshop.Models;
using Eshop.Models.Admin.Category;

namespace Eshop.Infrastructure.Orchestrator
{
    public interface ICategoryOrchestrator
    {
        IList<CategoryViewModel> GetAllCategories();
        OperationResult CreateCategory(CategoryEditCreateModel model);
        OperationResult DeleteCategory(int id);
        OperationResult UpdateCategory(CategoryEditCreateModel model);
    }
}
