﻿using Eshop.Models.Eshop;
using System.Collections;
using System.Collections.Generic;

namespace Eshop.Infrastructure.Orchestrator
{
    public interface IEshopOrchestrator
    {
        EshopViewModel GetEshopData();
        OperationResult GetProductModel(int id);
    }
}