﻿using Dapper;
using Eshop.Core.Database;
using Eshop.Models.Eshop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Infrastructure.Orchestrator
{
    public class EshopOrchestrator : IEshopOrchestrator
    {
        private readonly IDataProvider<FullProduct> _eshopProvider;
        private readonly CategoryProvider _categoryProvider;
        private readonly ProductProvider _productProvider;
        private readonly ProductImageProvider _productImageProvider;

        public EshopOrchestrator()
        {
            _eshopProvider = new DataProvider<FullProduct>("");
            _categoryProvider = new CategoryProvider();
            _productProvider = new ProductProvider();
            _productImageProvider = new ProductImageProvider();
        }

        

        public EshopViewModel GetEshopData()
        {
            EshopViewModel result = null;
            try
            {
                var cats = _categoryProvider.GetNonEmptyCategories();
                var prods = _productProvider.GetCollection("Active", "1");


                return new EshopViewModel()
                {
                    CategoryProductView = cats
                    .Select
                    (t => new Tuple<string, IList<ProductEshopViewModel>>(
                        t.Name,
                        prods
                            .Where(q => q.CategoryId == t.Id)
                            .Select(q => new ProductEshopViewModel()
                            {
                                Id = q.Id,
                                Name = q.Name,
                                ThumbnailImagePath = _productImageProvider.GetCollection("ProductId", q.ProductId.ToString()).FirstOrDefault()?.Path,
                                Price = q.Price,
                                PriceVAT = q.PriceDPH,
                                Description = q.Description
                            }).ToList()
                    )).ToList()
                };


                //var firstCatId = cats.FirstOrDefault().Id;
                //var categories = ProcessCategories(cats, activeCategory: firstCatId);

                //result = new EshopViewModel()
                //{
                //    Categories = categories,
                //    Products = prods
                //    .Select(t => new ProductEshopViewModel()
                //    {
                //        Id = t.Id,
                //        Name = t.Name,
                //        ThumbnailImagePath = _productImageProvider.GetCollection("ProductId", t.ProductId.ToString(), true).FirstOrDefault()?.Path,
                //        Price = t.Price,
                //        PriceVAT = t.PriceDPH,
                //        Description = t.Description

                //    }).ToList()
                //};
            }
            catch (Exception ex)
            {

                throw;
            }
            
            return null;
        }

        public OperationResult GetProductModel(int id)
        {
            ProductEshopViewModel model = null;

            try
            {
                //  TODO: refactor - single sql call
                var product = _productProvider.GetSingle(id);
                
                if(product != null)
                {
                    var cats = _categoryProvider.GetSingle(product.CategoryId);
                    model = new ProductEshopViewModel(product);
                    model.CategoryName = cats.Name;

                    // Get product images
                    model.Images = _productImageProvider.GetCollection("ProductId", product.ProductId.ToString())?
                        .Select(t => new Image
                        {
                            Name = t.Name,
                            Path = t.Path
                        }).ToList()
                        ?? new List<Image>() { new Image() { Name = "NoImage", Path = "/images/no-image.png" } };

                    // Get product options

                    return OperationResult.IsSuccess(model);
                }
            }
            catch (Exception ex)
            {
                //  TODO: LOG
            }
            return OperationResult.IsFailure();
        }

        private List<Models.Eshop.Category> ProcessCategories(IList<Core.Database.Category> cats, int parentId = 0, int activeCategory = 0)
        {
            return cats.Where(t => t.ParentCategoryId == parentId).Select(t => new Models.Eshop.Category()
            {
                Id = t.Id,
                Name = t.Name,
                Active = t.Id == activeCategory,
                ChildCategories = ProcessCategories(cats, t.Id)

            }).ToList();
        }
    }
}
