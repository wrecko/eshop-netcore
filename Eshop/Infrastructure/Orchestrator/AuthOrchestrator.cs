﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Core;
using Eshop.Core.Database;
using Eshop.Models;
using Microsoft.AspNetCore.Http;

namespace Eshop.Infrastructure.Orchestrator
{
    public class AuthOrchestrator : IAuthOrchestrator
    {
        private readonly UserProvider _userProvider;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public AuthOrchestrator(IHttpContextAccessor contextAccessor)
        {
            _httpContextAccessor = contextAccessor;
            _userProvider = new UserProvider();
        }

        public OperationResult ValidateLogin(LoginModel model)
        {
            try
            {
                var dbUser = _userProvider.GetSingle("login", model.Username);

                if (dbUser == null)
                    return OperationResult.IsFailure("User doesn't exists!!!");

                if(dbUser.Password == Hash.HashPassword(model.Password, dbUser.Salt).Item1)
                {
                    _session.SetObjectAsJson("USER", new UserSessionModel(dbUser.Login, (UserRoleEnum)dbUser.Role, _session.Id));
                    return OperationResult.IsSuccess();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            
            return OperationResult.IsFailure("Invalid credentials!!!");
        }
    }
}
