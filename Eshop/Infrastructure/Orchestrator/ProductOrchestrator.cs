﻿using Dapper;
using Eshop.Core.Database;
using Eshop.Core.Database.Entity;
using Eshop.Core.Database.Managers;
using Eshop.Models.Admin.Product;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ProductOptionRelation = Eshop.Core.Database.Entity.ProductOptionRelation;

namespace Eshop.Infrastructure.Orchestrator
{
    public class ProductOrchestrator : IProductOrchestrator
    {
        private const int TYPE_RELATION_MULTIPLIER = 100;

        private readonly ProductProvider _productProvider;
        private readonly CategoryProvider _categoryProvider;
        private readonly ProductImageProvider _productImageProvider;
        private readonly DataProvider<ProductOptionType> _productOptionTypeProvider;
        private readonly DataProvider<ProductOption> _productOptionProvider;
        private readonly DataProvider<ProductOptionRelation> _productOptionRelationProvider;
        private readonly DataProvider<ProductQuantity> _productQuantityProvider;
       

        #region ctor
        public ProductOrchestrator()
        {
            _productProvider = new ProductProvider();
            _categoryProvider = new CategoryProvider();
            _productImageProvider = new ProductImageProvider();
            _productOptionTypeProvider = new DataProvider<ProductOptionType>("[eshop].[ProductOptionType]");
            _productOptionProvider = new DataProvider<ProductOption>("[eshop].[ProductOption]");
            _productQuantityProvider = new DataProvider<ProductQuantity>("[eshop].[ProductQuantity]");
            _productOptionRelationProvider = new DataProvider<ProductOptionRelation>("[eshop].[ProductOptionRelation]");
        }
        #endregion

        #region Get Product
        public IList<ProductViewModel> GetProducts()
        {
            IList<Product> products = _productProvider.GetCollection();
            var categories = _categoryProvider.GetCollection();

            return products.Select(t => new ProductViewModel()
            {
                Id = t.Id,
                Name = t.Name,
                CategoryName = categories.Where(q => q.Id == t.CategoryId).Select(q => q.Name).FirstOrDefault(),
                Price = t.Price.ToString(),
                PriceDPH = t.PriceDPH.ToString(),
                Active = t.Active
            }).ToList();
        }
        public ProductEditCreateModel GetProduct(int id)
        {
            try
            {
                //  Get Categories
                var categories = _categoryProvider.GetCollection();
                //  Get product
                var product = _productProvider.GetSingle(id);
                if (product != null)
                {
                    //  Product images
                    var prodImages = _productImageProvider.GetCollection("ProductId", product.ProductId.ToString());

                    //  Product quantities
                    var quantity = _productQuantityProvider.GetCollection("ProductId", product.Id.ToString());
                    var options = _productOptionProvider.GetCollection();

                    List<ProductOptionRelationModel> quantities = null;
                    if (quantity != null && quantity.Count > 0)
                    {
                        //  Quantity created with relation option
                        if (!quantity.Any(t => t.ParentOptionId == null))
                        {
                            var optionRelation = _productOptionRelationProvider.GetSingle(quantity.First().OptionRelationId.Value);

                            var groupedQuantities = quantity.GroupBy(t => t.ParentOptionId);

                            quantities = options
                                .Where(t => t.OptionTypeId == optionRelation.ParentTypeId)
                                .Select(t => new ProductOptionRelationModel
                                {
                                    ParentOptionId = t.Id,
                                    Name = t.Name,
                                    Options = options
                                        .Where(q => q.OptionTypeId == optionRelation.ChildTypeId)
                                        .Select(q => new Option
                                        {
                                            Id = q.Id,
                                            Name = q.Name,
                                            Value = quantity.Where(w => w.OptionId == q.Id && w.ParentOptionId == t.Id).Select(w => w.Quantity).FirstOrDefault(),
                                            QuantityId = quantity.Where(w => w.OptionId == q.Id && w.ParentOptionId == t.Id).Select(w => w.Id).FirstOrDefault()
                                        }).ToList()
                                }).ToList();

                        }
                        else
                        {
                            var tmpQnt = quantity.FirstOrDefault();

                            var optionType = _productOptionTypeProvider
                                .GetCollection()
                                .Where(q => q.Id == options.Where(w => w.Id == tmpQnt.OptionId).Select(w => w.OptionTypeId).FirstOrDefault())
                                .FirstOrDefault();

                            quantities = new List<ProductOptionRelationModel>()
                            {
                                new ProductOptionRelationModel
                                {
                                    Name = optionType.Name,
                                    Options = options
                                    .Where(q => q.OptionTypeId == optionType.Id)
                                    .Select(q => new Option
                                    {
                                        Id = q.Id,
                                        Name = q.Name,
                                        Value = quantity.Where(w => w.OptionId == q.Id).Select(w => w.Quantity).FirstOrDefault(),
                                        QuantityId = quantity.Where(w => w.OptionId == q.Id).Select(w => w.Id).FirstOrDefault()
                                    }).ToList()
                                }
                            };
                        }
                    }

                    return new ProductEditCreateModel
                    {
                        Id = id,
                        Name = product.Name,
                        Active = product.Active,
                        Categories = categories.Select(t => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                        {
                            Text = t.Name,
                            Value = t.Id.ToString(),
                            Selected = t.Id == product.CategoryId
                        }).ToList(),
                        Description = product.Description,
                        Price = product.Price,
                        PriceVAT = product.PriceDPH,
                        TempImageID = product.ProductId,
                        Images = prodImages?.Select(t => new Photo
                        {
                            Id = t.Id,
                            Name = t.Name,
                            Path = t.Path
                        }).ToList(),
                        Options = quantities
                    };
                }
            }
            catch (Exception ex)
            {
                //  TODO: LOG                
            }
            return null;
        }

        public ProductEditCreateModel GetDataForProductCreate()
        {
            var categories = _categoryProvider.GetCollection();

            var options = _productOptionTypeProvider.GetCollection($"select [Id], [Name] from {_productOptionTypeProvider.TableName} union select Id*{TYPE_RELATION_MULTIPLIER} as Id,[Name] from {_productOptionRelationProvider.TableName}", new DynamicParameters());

            return new ProductEditCreateModel()
            {
                TempImageID = Guid.NewGuid(),
                Categories = categories.Select(t => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                {
                    Text = t.Name,
                    Value = t.Id.ToString()
                }).ToList(),
                OptionTypes = options.Select(t => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Text = t.Name,
                    Value = t.Id.ToString()
                }).ToList()
            };
        }
        #endregion

        #region Create product

        public OperationResult CreateProduct(ProductEditCreateModel model)
        {
            try
            {
                //  Save product
                var id = _productProvider.Create(new Product
                {
                    Name = model.Name,
                    Active = model.Active,
                    CategoryId = model.Category,
                    Description = model.Description,
                    Price = model.Price,
                    PriceDPH = model.PriceVAT,
                    ProductId = model.TempImageID
                    //URL = GenerateProductURL
                });

                //  Save images          
                if (model.Images != null && model.Images.Count > 0)
                    _productImageProvider.Create(model.Images.Select(t => new Core.Database.Entity.ProductImage()
                    {
                        ProductID = model.TempImageID,
                        Name = t.Name,
                        Path = t.Path
                    }).ToArray());

                //  Save Quantities
                if (model.Quantities != null && model.Quantities.Count > 0)
                    foreach (var item in model.Quantities.Where(t => t.Value != null))
                    {
                        _productQuantityProvider.Create(new ProductQuantity {
                            ProductId = id,
                            OptionId = item.OptionId,
                            Quantity = item.Value.Value,
                            ParentOptionId = item.ParentOptionId,
                            OptionRelationId = model.OptionRelationId/TYPE_RELATION_MULTIPLIER
                        });
                    }
            }
            catch (Exception ex)
            {
                //  TODO: LOG
                return OperationResult.IsFailure();
            }

            return OperationResult.IsSuccess();
        }
        #endregion

        #region Update product
        public OperationResult UpdateProduct(ProductEditCreateModel model, IHostingEnvironment env)
        {
            try
            {
                //  Update product
                var resP = _productProvider.Update(new Product
                {
                    Id = model.Id,
                    Active = model.Active,
                    CategoryId = model.Category,
                    Description = model.Description,
                    Name = model.Name,
                    Price = model.Price,
                    PriceDPH = model.PriceVAT,
                    ProductId = model.TempImageID
                    //URL =
                });

                //  Update images
                var images = _productImageProvider.GetCollection("ProductId", model.TempImageID.ToString()); 
                foreach (var image in images)
                {
                    if (model.Images == null || !model.Images.Any(t => t.Id == image.Id))
                    {
                        //  Delete from DB
                        _productImageProvider.Delete(image.Id);

                        //  Delete from file system
                        var uploads = System.IO.Path.Combine(env.WebRootPath, "uploads", model.TempImageID.ToString());
                        var exists = System.IO.File.Exists($"{uploads}\\{image.Name}");
                        if (exists)
                            System.IO.File.Delete($"{uploads}\\{image.Name}");
                        
                        //  If directory is empty delete directory
                        if (System.IO.Directory.GetFiles(uploads)?.Length == 0)
                        {
                            Directory.Delete(uploads, false);
                        }
                    }
                }

                if(model.Images != null)
                    foreach (var image in model.Images)
                    {
                        if(image.Id == 0)
                            _productImageProvider.Create(new ProductImage
                            {
                                Name = image.Name,
                                Path = image.Path,
                                ProductID = model.TempImageID                        
                            });
                    }

                //  Update options
                if(model.Quantities != null)
                {
                    var savedQuantities = _productQuantityProvider.GetCollection("ProductId", model.Id.ToString());
                    foreach (var item in model.Quantities)
                    {
                        if (@savedQuantities.Any(t => t.Id == item.QuantityId && t.Quantity == item.Value))
                            continue;

                        var productQuantity = new ProductQuantity
                        {
                            Id = item.QuantityId.Value,
                            Quantity = item.Value.Value
                        };

                        if (item.QuantityId != 0)
                            _productQuantityProvider.Update(productQuantity, true);
                        else
                        {
                            productQuantity.ProductId = model.Id;
                            productQuantity.OptionRelationId = model.OptionRelationId;
                            productQuantity.OptionId = item.OptionId;
                            productQuantity.ParentOptionId = item.ParentOptionId;
                            _productQuantityProvider.Create(productQuantity);
                        }
                    }
                }                

                return OperationResult.IsSuccess();
            }
            catch (Exception ex)
            {
                //  TODO: LOG                
            }
            return OperationResult.IsFailure();
        }

        #endregion

        #region Delete product
        public OperationResult DeleteProduct(int id)
        {
            try
            {
                var product = _productProvider.GetSingle(id);
                if (product == null)
                    return OperationResult.IsFailure("Product doesn't exist!!!");

                var query = "DELETE FROM {0} WHERE ProductId = @val";
                var param = new Dapper.DynamicParameters();
                param.Add("@val", product.Id);

                _productQuantityProvider.ExecuteQuery(String.Format(query, _productQuantityProvider.TableName), param);

                param = new Dapper.DynamicParameters();
                param.Add("@val", product.ProductId);

                _productImageProvider.ExecuteQuery(String.Format(query, _productImageProvider.TableName), param);

                _productProvider.Delete(id);

                return OperationResult.IsSuccess();
            }
            catch (Exception ex)
            {
                //TODO: LOG
            }
            return OperationResult.IsFailure();
        }

        #endregion

        #region Product Options

        #region  GET        
        //  TODO: Maybe operation result?
        //  TODO: REFACTOR!!!!!
        public IList<ProductOptionRelationModel> GetProductOptions(int typeId = 0)
        {
            try
            {
                //  If is type relation
                if(typeId % TYPE_RELATION_MULTIPLIER == 0)
                {
                    var relation = _productOptionRelationProvider.GetSingle(typeId / 100);
                    if (relation == null)
                        return null;
                    
                    var options = _productOptionProvider.GetCollection();
                    var parentOptions = options.Where(t => t.OptionTypeId == relation.ParentTypeId).ToList(); 
                    var childOptions = options.Where(t => t.OptionTypeId == relation.ChildTypeId).ToList();

                    return parentOptions.Select(t => new ProductOptionRelationModel 
                    {
                        ParentOptionId = t.Id, 
                        Name = t.Name, 
                        Options = childOptions.Select(q => new Option { Id = q.Id, Name = q.Name } ).ToList() 
                    }).ToList();

                }
                var otions = _productOptionProvider.GetCollection();
                if (typeId != 0)
                {
                    //  REFACTOR
                    var optionTypeName = _productOptionTypeProvider.GetCollection("Id", typeId.ToString()).Select(t => t.Name).FirstOrDefault();
                    return new List<ProductOptionRelationModel>
                    { 
                        new ProductOptionRelationModel { Name = optionTypeName, Options = otions.Where(t => t.OptionTypeId == typeId).Select(q => new Option { Id = q.Id, Name = q.Name }).ToList() }
                    };
                }

                return new List<ProductOptionRelationModel>
                {
                    new ProductOptionRelationModel { Options = otions.Select(q => new Option { Id = q.Id, Name = q.Name }).ToList() }
                };                                 
            }
            catch (Exception)
            {
                //  TODO: LOG                
            }
            return null;
        }
        public ProductOptionViewModel GetOptionsViewModel()
        {
            try
            {
                //  Get all option types
                var optionTypes = _productOptionTypeProvider.GetCollection();

                //  Get Options
                var options = _productOptionProvider.GetCollection();

                //  Get Option Relations
                var relations = _productOptionRelationProvider.GetCollection();

                return new ProductOptionViewModel()
                {
                    OptionTypes = optionTypes.Select(t => new ProductOptionView {
                        Id = t.Id,
                        Name = t.Name
                    }).ToList(),
                    Options = options.Select(t => new ProductOptionView
                    {
                        Id = t.Id,
                        Name = t.Name,
                        OptionType = optionTypes.Where(q => q.Id == t.OptionTypeId).Select(q => q.Name).FirstOrDefault(),
                        OptionTypeId = t.OptionTypeId
                    }).ToList(),
                    OptionRelations = relations.Select(t => new ProductRelationView
                    {
                        Id = t.Id,
                        Name = t.Name
                    }).ToList()
                };
            }
            catch (Exception)
            {
                //  TODO: LOG
            }

            return null;
        }

        #endregion

        #region Product Option Type


        public OperationResult CreateProductOptionType(ProductOptionView model)
        {
            try
            {
                //var categories = _categoryProvider.GetCollection();

                _productOptionTypeProvider.Create(new ProductOptionType
                {
                    Name = model.Name,
                    CategoryId = null
                });
            }
            catch (Exception ex)
            {
                return OperationResult.IsFailure();
            }
            return OperationResult.IsSuccess();
        }

        public OperationResult DeleteProductOptionType(int id)
        {
            try
            {
                if (_productOptionTypeProvider.Delete(id))
                {
                    _productOptionProvider.ExecuteQuery($"delete from {_productOptionProvider.TableName} where OptionTypeId = {id} ", null);
                    return OperationResult.IsSuccess();
                }


            }
            catch (Exception ex)
            {
                //TODO: LOG
            }

            return OperationResult.IsFailure();
        }

        public OperationResult EditProductOptionType(ProductOptionView model)
        {
            try
            {
                var categories = _categoryProvider.GetCollection();

                _productOptionTypeProvider.Update(new ProductOptionType
                {
                    Id = model.Id,
                    Name = model.Name,
                    //CategoryId = model.CategoryId
                });
            }
            catch (Exception ex)
            {
                return OperationResult.IsFailure();
            }
            return OperationResult.IsSuccess();
        }

        #endregion

        #region Product Option

        public OperationResult CreateProductOption(ProductOptionView model)
        {
            try
            {
                _productOptionProvider.Create(new ProductOption
                {
                    Name = model.Name,
                    OptionTypeId = model.OptionTypeId

                });
            }
            catch (Exception ex)
            {
                return OperationResult.IsFailure();
            }
            return OperationResult.IsSuccess();
        }

        public OperationResult EditProductOption(ProductOptionView model)
        {
            try
            {
                _productOptionProvider.Update(new ProductOption
                {
                    Id = model.Id,
                    Name = model.Name,
                    OptionTypeId = model.OptionTypeId
                });
            }
            catch (Exception ex)
            {
                return OperationResult.IsFailure();
            }
            return OperationResult.IsSuccess();
        }
        public OperationResult DeleteProductOption(int id)
        {
            try
            {
                if (_productOptionProvider.Delete(id))
                    return OperationResult.IsSuccess();
            }
            catch (Exception ex)
            {
                //TODO: LOG
            }

            return OperationResult.IsFailure();
        }

        #endregion

        #region Product option relation
        public OperationResult CreateProductOptionRelation(ProductRelationView model)
        {         
            try
            {
                _productOptionRelationProvider.Create(new ProductOptionRelation{
                    Name = model.Name,
                    ChildTypeId = model.ChildOptionId,
                    ParentTypeId = model.ParentOptionId
                });
            }
            catch (Exception ex)
            {
                return OperationResult.IsFailure();
            }
            return OperationResult.IsSuccess();
        }

        public OperationResult DeleteProductOptionRelation(int id)
        {
            try
            {
                _productOptionRelationProvider.Delete(id);
            }
            catch (Exception ex)
            {
                return OperationResult.IsFailure();
            }
            return OperationResult.IsSuccess();
        }

        #endregion
        
        #endregion
    }
}
