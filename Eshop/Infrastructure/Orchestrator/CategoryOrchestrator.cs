﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Core.Database;
using Eshop.Models;
using Eshop.Models.Admin.Category;

namespace Eshop.Infrastructure.Orchestrator
{
    public class CategoryOrchestrator : ICategoryOrchestrator
    {
        private readonly CategoryProvider _provider;

        public CategoryOrchestrator()
        {
            this._provider = new CategoryProvider();
        }
        
        public IList<CategoryViewModel> GetAllCategories()
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            var dbList = _provider.GetCollection();
            foreach (var item in dbList)
            {
                result.Add(new CategoryViewModel()
                {
                    Id = item.Id,
                    Name = item.Name,
                    ParentCategoryId = item.ParentCategoryId,
                    ParentCategoryName = item.ParentCategoryId != 0? dbList.Where(t => t.Id == item.ParentCategoryId).FirstOrDefault().Name : ""
                });
            }

            return result;
        }
        
        public OperationResult CreateCategory(CategoryEditCreateModel model)
        {
            try
            {
                var result = _provider.Create(new Category()
                {
                    Name = model.Name,
                    ParentCategoryId = model.ParentCategoryId
                });

                if (result > 0)
                    return OperationResult.IsSuccess();

            }
            catch (Exception ex)
            {
                //  LOG
            }

            return OperationResult.IsFailure("Something went bad!!!");
        }

        public OperationResult DeleteCategory(int id)
        {
            try
            {
                var result = _provider.Delete(id);
                _provider.ExecuteQuery($"delete from {_provider.TableName} where ParentCategoryId = {id}", new Dapper.DynamicParameters());

                if (result)
                    return OperationResult.IsSuccess();

            }
            catch (Exception ex)
            {
                //  LOG
            }

            return OperationResult.IsFailure("Something went bad!!!");
        }

        public OperationResult UpdateCategory(CategoryEditCreateModel model)
        {
            try
            {
                var result = _provider.Update(new Category()
                {
                    Id = model.Id,
                    Name = model.Name,
                    ParentCategoryId = model.ParentCategoryId
                });

                if (result)
                    return OperationResult.IsSuccess();

            }
            catch (Exception ex)
            {
                //  LOG
            }

            return OperationResult.IsFailure("Something went bad!!!");
        }
    }
}
