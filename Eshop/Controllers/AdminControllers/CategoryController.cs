﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Infrastructure.Orchestrator;
using Eshop.Models;
using Eshop.Models.Admin.Category;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Eshop.Controllers.EshopControllers
{
    public class CategoryController : AdminBaseController
    {
        private readonly ICategoryOrchestrator _orchestrator;

        public CategoryController(ICategoryOrchestrator orchestrator)
        {
            this._orchestrator = orchestrator;            
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var model = _orchestrator.GetAllCategories();
            return ToView(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var categories = _orchestrator.GetAllCategories();

            var model = new CategoryEditCreateModel() { CategoryNames = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>() { new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem() { Value = "0", Text = "" } } };
            foreach (var item in categories)
            {
                model.CategoryNames.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem() {
                    Text = item.Name,
                    Value = item.Id.ToString()                    
                });
            }


            return ToView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CategoryEditCreateModel model)
        {
            //if(ModelState.IsValid)
            //{
            //    var result = _orchestrator.CreateCategory(model);
            //    if (result.Success)
            //        return RedirectToAction("Index");
            //}
            var result = _orchestrator.CreateCategory(model);
            if (result.Success)
                return RedirectToAction("Index");

            return ToView(model);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            Infrastructure.OperationResult result = _orchestrator.DeleteCategory(id);
            if (result.Success)
                return RedirectToAction("Index");

            return ToView();
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var categories = _orchestrator.GetAllCategories();
            var category = categories.Where(t => t.Id == id).FirstOrDefault();

            var model = new CategoryEditCreateModel() {
                Id = category.Id,
                Name = category.Name,
                ParentCategoryId = category.ParentCategoryId,                
                CategoryNames = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>() {
                    new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem() { Value = "0", Text = "" }
                }
            };
            
            foreach (var item in categories)
            {
                model.CategoryNames.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                {
                    Text = item.Name,
                    Value = item.Id.ToString(),
                    Selected = model.ParentCategoryId == item.Id
                });
            }

            return ToView(model, "Create");
        }

        [HttpPost]
        public IActionResult Update(CategoryEditCreateModel model)
        {
            Infrastructure.OperationResult result = _orchestrator.UpdateCategory(model);
            if (result.Success)
                return RedirectToAction("Index");

            return ToView(model, "Create");
        }

    }
}
