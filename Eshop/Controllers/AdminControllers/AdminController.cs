﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Infrastructure.Orchestrator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Eshop.Controllers
{
    
    public class AdminController : AdminBaseController
    {
        private readonly IAdminOrchestrator orchestrator;

        public AdminController(IAdminOrchestrator orchestrator)
        {
            this.orchestrator = orchestrator;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            
            
            return View();
        }
    }
}
