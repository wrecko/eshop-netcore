﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Infrastructure;
using Eshop.Infrastructure.Orchestrator;
using Eshop.Models.Admin.Product;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Eshop.Controllers.AdminControllers
{
    public class ProductController : AdminBaseController
    {
        private readonly IProductOrchestrator _orchestrator;
        private readonly IHostingEnvironment _env;

        public ProductController(IProductOrchestrator orchestrator, IHostingEnvironment env)
        {
            this._orchestrator = orchestrator;
            this._env = env;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {           
            IList<ProductViewModel> model = _orchestrator.GetProducts();
            return ToView(model);
        }
        #region Products

        [HttpGet]
        public IActionResult Create()
        {
            ProductEditCreateModel model = _orchestrator.GetDataForProductCreate();
            
            return ToView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ProductEditCreateModel model)
        {
            if(ModelState.IsValid)
            {
                OperationResult result = _orchestrator.CreateProduct(model);
                if (result.Success)
                    return RedirectToAction("Index");
            }

            var data = _orchestrator.GetDataForProductCreate();
            model.Categories = data.Categories;
            model.OptionTypes = data.OptionTypes;

            return ToView(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveProductImages()
        {            
            var files = HttpContext.Request.Form.Files;
            var tempId = HttpContext.Request.Form.Where(t => t.Key == "TempID").Select(t => t.Value).FirstOrDefault().ToString().Split(',')[0];
            
            var uploads = Path.Combine(_env.WebRootPath, "uploads", tempId);
            if (!Directory.Exists(uploads))
                Directory.CreateDirectory(uploads);

            List<Photo> images = new List<Photo>();
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    var path = Path.Combine(uploads, file.FileName);
                    using (var fileStream = new FileStream(path, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                        images.Add(new Photo()
                        { 
                            Name = file.FileName,
                            Path = path.Substring(path.IndexOf("wwwroot")+ "wwwroot".Length)
                        });
                    }
                }
            }            

            return Json(new { images });
        }
        [HttpPost]
        public IActionResult DeleteSavedImage(string name, string tempID)
        {
            var uploads = Path.Combine(_env.WebRootPath, "uploads", tempID);
            var exists = System.IO.File.Exists($"{uploads}\\{name}");
            if (exists)
                System.IO.File.Delete($"{uploads}\\{name}");

            return Json(new { });
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            ProductEditCreateModel model = _orchestrator.GetProduct(id);
            if (model == null)
                return RedirectToAction("Index");

            model.IsUpdate = true;
            return ToView(model, "Create");
        }

        [HttpPost]
        public IActionResult Update(ProductEditCreateModel model)
        {
            OperationResult result = _orchestrator.UpdateProduct(model, _env);
            if (result.Success)
                return RedirectToAction("Index");
            return ToView(model, "Create");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            OperationResult result = _orchestrator.DeleteProduct(id);
            return RedirectToAction("Index");
        }

        #endregion

        #region Product Options
        [HttpGet]
        public IActionResult Options()
        {
            var model = _orchestrator.GetOptionsViewModel();
            return ToView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OptionTypeCreate(ProductOptionView model)
        {
            if(ModelState.IsValid)
            {
                OperationResult result = _orchestrator.CreateProductOptionType(model);                
            }
            return RedirectToAction("Options");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OptionTypeEdit(ProductOptionView model)
        {
            if (ModelState.IsValid)
            {
                OperationResult result = _orchestrator.EditProductOptionType(model);
            }
            return RedirectToAction("Options");
        }

        [HttpGet]
        public IActionResult OptionTypeDelete(int Id)
        {
            //  TODO: add error msgs
            if (Id > 0)
                if (!_orchestrator.DeleteProductOptionType(Id).Success)
                    return ToView(view: "Options");

            return RedirectToAction("Options");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OptionCreate(ProductOptionView model)
        {
            if (ModelState.IsValid)
            {
                OperationResult result = _orchestrator.CreateProductOption(model);
            }
            return RedirectToAction("Options");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OptionEdit(ProductOptionView model)
        {
            if (ModelState.IsValid)
            {
                OperationResult result = _orchestrator.EditProductOption(model);
            }
            return RedirectToAction("Options");
        }

        [HttpGet]
        public IActionResult OptionDelete(int Id)
        {
            //  TODO: add error msgs
            if (Id > 0)
                if (!_orchestrator.DeleteProductOption(Id).Success)
                    return ToView(view: "Options");

            return RedirectToAction("Options");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult OptionRelationCreate(ProductRelationView model)
        {
            if (ModelState.IsValid && model.ParentOptionId != model.ChildOptionId)
            {
                OperationResult result = _orchestrator.CreateProductOptionRelation(model);
            }
            return RedirectToAction("Options");
        }

        [HttpGet]
        public IActionResult OptionRelationDelete(int Id)
        {
            //  TODO: add error msgs
            if (Id > 0)
                if (!_orchestrator.DeleteProductOptionRelation(Id).Success)
                    return ToView(view: "Options");

            return RedirectToAction("Options");
        }

        #endregion

        #region Ajax methods
        [HttpPost]
        public IActionResult GetOptionsForType(int optionTypeId)
        {
            if(optionTypeId > 0)
                return Json(new { options = _orchestrator.GetProductOptions(optionTypeId) });
            
            return Json(new {  });
        }

        #endregion

    }
}
