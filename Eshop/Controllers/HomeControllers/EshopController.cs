﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Core.Database;
using Eshop.Infrastructure;
using Eshop.Infrastructure.Orchestrator;
using Eshop.Models.Eshop;
using Microsoft.AspNetCore.Mvc;

namespace Eshop.Controllers.HomeControllers
{
    public class EshopController : BaseController
    {
        private readonly IEshopOrchestrator _orchestrator;

        public EshopController(IEshopOrchestrator orchestrator)
        {
            _orchestrator = orchestrator;
        }
        public IActionResult Index()
        {
            var model = _orchestrator.GetEshopData();
            return View(model);
        }

        public IActionResult Detail(int id)
        {
            //OperationResult result = _orchestrator.GetProductModel(id);
            var result = OperationResult.IsSuccess(new ProductEshopViewModel
            {
                Id = id,
                Name = "Dummy name",
                PriceVAT = 99,
                Description = "Vec ktoru si chces silno kupit ale aj tak si to nekusis lebo si dilino",
                Images = new List<Image>() { new Image() { Name = "Noimga", Path = "/images/no-image.png" } },
                CategoryName = "Trenirky"
            });
            return View(result.Model);
        }
    }
}