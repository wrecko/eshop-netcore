﻿using Eshop.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Controllers
{
    public abstract class BaseController : Controller 
    {
        protected string Layout { get; set; }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            ViewData["Title"] = ConfigManager.Get("PageTitle");
            ViewData["Layout"] = Layout;

            base.OnActionExecuted(context);
        }

        protected ViewResult ToView(object model = null, string view = "")
        {
            StackTrace stackTrace = new StackTrace();
            var action = string.IsNullOrEmpty(view)? stackTrace.GetFrame(1).GetMethod().Name : view;
            var controller = this.GetType().Name.Replace("Controller", "");

            return View($"/Views/Admin/{controller}/{action}.cshtml", model);
        }

        
    }
}
