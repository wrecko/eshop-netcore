﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eshop.Infrastructure.Orchestrator;
using Eshop.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Eshop.Controllers
{
    public class AuthController : BaseController
    {
        private readonly IAuthOrchestrator _orchestrator;

        public AuthController(IAuthOrchestrator orchestrator)
        {
            _orchestrator = orchestrator;
        }

        [HttpGet]
        [Route("login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Route("login")]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginModel model)
        {
            if(ModelState.IsValid)
            {
                var result = _orchestrator.ValidateLogin(model);
                if (result.Success)
                    return Redirect("/Admin");
            }
            return View(model);
        }

        [HttpGet]
        [Route("logout")]
        public IActionResult Logout()
        {
            ControllerContext.HttpContext.Session.Clear();            
            return RedirectToAction("Login");
        }
    }
}
