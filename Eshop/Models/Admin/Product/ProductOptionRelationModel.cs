﻿using Eshop.Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Models.Admin.Product
{
    public class ProductOptionRelationModel
    {
        public string Name { get; set; }
        public int? ParentOptionId { get; set; }

        public IList<Option> Options {get; set;}
    }

    public class Option
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }

        public int? QuantityId { get; set; }
    }

}
