﻿using Eshop.Core;
using Eshop.Core.Database.Entity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Models.Admin.Product
{
    public class ProductEditCreateModel : BaseModel
    {        
        public bool Active { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        //[RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers allowed!")]
        public double Price { get; set; }
        
        [Required]
        //[RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers allowed!")]
        public double PriceVAT { get; set; }
        
        [Required]
        public int Category { get; set; }

        public IList<Quantity> Quantities { get; set; }
        public IList<Photo> Images { get; set; }

        [Required]
        public Guid TempImageID { get; set; }

        public IList<SelectListItem> OptionTypes { get; set; }        
        public IList<SelectListItem> Categories { get; set; }

        public IList<ProductOptionRelationModel> Options { get; set; }
        public int OptionRelationId { get; set; }


        public string VAT { get { return ConfigManager.Get("VAT"); } }
        public bool IsUpdate { get; set; }

    }

    public class Photo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        //public bool IsThumbnal { get; set; }
    }

    public class Quantity
    {
        public int? QuantityId { get; set; }
        public int? Value { get; set; }
        public int OptionId { get; set; }
        public int? ParentOptionId { get; set; }
    }

}
