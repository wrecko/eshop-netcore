﻿using Eshop.Core.Database.Entity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Models.Admin.Product
{
    public class ProductOptionViewModel
    {
        public IList<ProductOptionView> Options { get; set; }

        public IList<ProductOptionView> OptionTypes { get; set; }

        public IList<ProductRelationView> OptionRelations { get; set; }

        public IList<SelectListItem> OptionTypesList
        {
            get 
            {
                return OptionTypes.Select(t => new SelectListItem
                {
                    Text = t.Name,
                    Value = t.Id.ToString()
                }).ToList(); 
            }
        }
    }

    public class ProductOptionView : BaseModel
    {
        [Required]
        public string Name { get; set; }

        public int OptionTypeId { get; set; }

        public string OptionType { get; set; }
        
        //public int CategoryId { get; set; }

        //public string Category { get; set; }
    }

    public class ProductRelationView : BaseModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(1,999)]        
        public int ParentOptionId { get; set; }

        [Required]
        [Range(1, 999)]
        public int ChildOptionId { get; set; }

    }
}
