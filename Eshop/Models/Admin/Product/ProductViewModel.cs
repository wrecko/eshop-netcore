﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Models.Admin.Product
{
    public class ProductViewModel : BaseModel
    {
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public bool Active { get; set; }
        public string Price { get; set; }
        public string PriceDPH { get; set; }
        public string ThumbnailImgPath { get; set; }
    }
}
