﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Models.Admin.Category
{
    public class CategoryEditCreateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentCategoryId { get; set; }

        public IList<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem> CategoryNames { get; set; }
    }
}
