﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Models.Admin.Category
{
    public class CategoryViewModel : BaseModel
    {
        //[Required]
        public string Name { get; set; }
        public int ParentCategoryId { get; set; }
        public string ParentCategoryName { get; set; }
        
    }
}
