﻿using Eshop.Core.Database;
using System.Collections.Generic;

namespace Eshop.Models.Eshop
{
    public class ProductEshopViewModel : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumbnailImagePath { get; set; }
        public IList<Image> Images { get; set; }

        public double Price { get; set; }
        public double PriceVAT { get; set; }

        public IList<string> Options { get; set; }
        public string CategoryName { get; set; }

        public ProductEshopViewModel(){}

        public ProductEshopViewModel(Product product)
        {
            Name = product.Name;
            Description = product.Description;
            Price = product.Price;
            PriceVAT = product.PriceDPH;
        }
    }
    
    public class Image
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}