﻿using Eshop.Core.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eshop.Models.Eshop
{
    public class EshopViewModel
    {
        public IList<Category> Categories { get; set; }
        public IList<ProductEshopViewModel> Products { get; set; }

        public IList<Tuple<string, IList<ProductEshopViewModel>>> CategoryProductView { get; set; }
    }

    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        public IList<Category> ChildCategories { get; set; }
    }
}
