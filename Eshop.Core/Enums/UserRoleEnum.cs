﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core
{
    public enum UserRoleEnum
    {
        ADMIN   = 1,
        USER    = 2
    }
}
