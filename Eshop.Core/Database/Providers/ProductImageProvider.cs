﻿using Eshop.Core.Database.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database
{
    public class ProductImageProvider : DataProvider<ProductImage>, IProductImageProvider
    {
        public ProductImageProvider()
            : base("[eshop].[ProductImage]")
        {
                
        }
    }
}
