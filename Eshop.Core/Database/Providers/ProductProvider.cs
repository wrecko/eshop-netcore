﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database
{
    public class ProductProvider : DataProvider<Product>, IProductProvider
    {
        public ProductProvider()
            : base("[eshop].[Product]")
        {

        }

    }
}
