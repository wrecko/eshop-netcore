﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database
{
    public class CategoryProvider : DataProvider<Category>, ICategoryProvider
    {
        public CategoryProvider()
            : base("[eshop].[ProductCategory]")
        {

        }


        public IList<Category> GetNonEmptyCategories()
        {
            return GetCollection($"select c.Id, c.Name from {TableName} c inner join [eshop].[Product] p on p.CategoryId = c.Id where p.active = 1 group by c.Id, c.Name", new DynamicParameters());
        }
    }
}
