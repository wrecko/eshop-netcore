﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database
{
    public class UserProvider : DataProvider<User>, IUserProvider
    {
        public UserProvider()
            : base("[dbo].[User]")
        {

        }
    }
}
