﻿using System.Collections.Generic;

namespace Eshop.Core.Database.Managers
{
    public interface IProductManager
    {
        IList<Product> GetProducts();
    }
}