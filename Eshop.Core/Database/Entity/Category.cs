﻿using Dapper.Contrib.Extensions;
using DapperExtensions.Mapper;

namespace Eshop.Core.Database
{
    [Table("ProductCategory")]    
    public class Category : BaseEntity
    {
        public string Name { get; set; }
        public int ParentCategoryId { get; set; }
    }

    public class CategoryMapper : AutoClassMapper<Category>
    {
        public CategoryMapper() : base()
        {
            Schema("eshop");
            Table("ProductCategory");            
        }
    }
}