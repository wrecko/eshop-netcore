﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database.Entity
{
    public class ProductOptionType : BaseEntity
    {
        public string Name { get; set; }
        public string CategoryId { get; set; }
    }

    public class ProductOptionTypeMapper : AutoClassMapper<ProductOptionType>
    {
        public ProductOptionTypeMapper() : base()
        {
            Schema("eshop");
            Table("ProductOptionType");
        }
    }
}
