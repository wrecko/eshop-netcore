﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database.Entity
{
    public class ProductImage : BaseEntity
    {
        public Guid ProductID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public bool IsThumbNail { get; set; }

    }

    public class ProductImageMapper : AutoClassMapper<ProductImage>
    {
        public ProductImageMapper() : base()
        {
            Schema("eshop");
            Table("ProductImage");
        }
    }
}
