﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Eshop.Core.Database
{
    [Table("[dbo].[User]")]
    public class User : BaseEntity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }

        public int Role { get; set; }
    }
}
