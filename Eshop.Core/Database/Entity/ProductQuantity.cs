﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database
{
    public class ProductQuantity : BaseEntity
    {
        public int OptionId { get; set; }
        public int? ParentOptionId { get; set; }
        public int? OptionRelationId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }

    public class ProductQuantityMapper : AutoClassMapper<ProductQuantity>
    {
        public ProductQuantityMapper() : base()
        {
            Schema("eshop");
            Table("ProductQuantity");
        }
    }
}
