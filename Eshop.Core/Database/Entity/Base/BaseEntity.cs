﻿using Dapper.Contrib.Extensions;
using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Eshop.Core.Database
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
        
        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public BaseEntity()
        {
            //CreatedOn = DateTime.UtcNow;
            //UpdatedOn = DateTime.UtcNow;
        }
    }    
}
