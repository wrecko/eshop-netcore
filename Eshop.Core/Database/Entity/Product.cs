﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database
{
    public class Product : BaseEntity
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public double PriceDPH { get; set; }
        public bool Active { get; set; }
        public string URL { get; set; }
        public Guid ProductId { get; set; }
    }

    public class ProductMapper : AutoClassMapper<Product>
    {
        public ProductMapper() : base()
        {
            Schema("eshop");
            Table("Product");
        }
    }
}
