﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database.Entity
{
    public class ProductOptionRelation : BaseEntity
    {
        public int ParentTypeId { get; set; }
        public int ChildTypeId { get; set; }
        public string Name { get; set; }
    }

    public class ProductOptionRelationMapper : AutoClassMapper<ProductOptionRelation>
    {
        public ProductOptionRelationMapper() : base()
        {
            Schema("eshop");
            Table("ProductOptionRelation");
        }
    }
}
