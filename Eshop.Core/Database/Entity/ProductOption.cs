﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core.Database.Entity
{
    public class ProductOption : BaseEntity
    {
        public int OptionTypeId { get; set; }
        public string Name { get; set; }        
    }

    public class ProductOptionMapper : AutoClassMapper<ProductOption>
    {
        public ProductOptionMapper() : base()
        {
            Schema("eshop");
            Table("ProductOption");
        }
    }
}
