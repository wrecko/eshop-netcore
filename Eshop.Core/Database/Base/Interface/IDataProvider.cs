﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Eshop.Core.Database
{
    public interface IDataProvider<T> where T : BaseEntity, new()
    {
        T GetSingle(int id);

        T GetSingle(string val1, string val2);

        Task<T> GetSingleAsync(int id);

        IList<T> GetCollection(string val1, string val2);

        IList<T> GetCollection(Expression<Func<T, bool>> predicate);

        IList<T> GetCollection(string sql, DynamicParameters parameters);

        Task<IEnumerable<T>> GetCollectionAsync(Expression<Func<T, bool>> predicate);

        bool Update(T model, bool onlyUpdated = false);

        Task<bool> UpdateAsync(T model, bool onlyUpdated = false);

        bool Delete(int id);

        Task<bool> DeleteAsync(int id);

        int Create(T model);

        void Create(IEnumerable<T> model);

        void CreateAsync(T model);

        void CreateAsync(IEnumerable<T> model);

        object ExecuteQuery(string sql, DynamicParameters parameter);

    }
}