﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using DapperExtensions;

namespace Eshop.Core.Database
{
    public class DbDataProvider<T> 
        where T : BaseEntity, new()
    {
        public string TableName { get; }

        public DbDataProvider(string tableName)
        {
            TableName = tableName;            
        }

        private SqlConnection GetSqlConnection() => new SqlConnection(ConfigManager.GetConnectionString("DatabaseConnectionString"));

        public T GetSingle(int id)
        {
            string sql = $"SELECT * FROM {this.TableName} WHERE Id = @Id";

            using (var connection = GetSqlConnection())
            {
                return connection.Get<T>(id);
            }
        }

        public T GetSingle(string val1, string val2)
        {
            return GetCollection(val1, val2)?[0] ?? null;
        }

        public Task<T> GetSingleAsync(int id)
        {
            string sql = $"SELECT * FROM {this.TableName} WHERE Id = @Id";

            using (var connection = GetSqlConnection())
            {
                return connection.GetAsync<T>(id);
            }
        }

        public IList<T> GetCollection(string val1 = "", string val2 = "")
        {
            using (var connection = GetSqlConnection())
            {
                string sql = $"select * from {this.TableName}";
                if (val1 != "" && val2 != "")
                {
                    sql += $"where {val1} = @val;";
                }


                return connection.QueryMultiple(sql, new { @val = val2 }).Read<T>().AsList();
            }
        }

        public IList<T> GetCollection(Expression<Func<T, bool>> predicate)
        {
            using (var connection = GetSqlConnection())
            {
                var tt = connection.QueryMultiple($"select * from {this.TableName} where login = 'test'").Read<T>().AsList();
                return connection.GetList<T>(predicate).AsList();
            }

        }

        public Task<IEnumerable<T>> GetCollectionAsync(Expression<Func<T, bool>> predicate)
        {
            using (var connection = GetSqlConnection())
            {
                return connection.GetListAsync<T>(predicate);
            }
        }

        public bool Update(T model)
        {
            model.UpdatedOn = DateTime.UtcNow;
            DynamicParameters parameter = new DynamicParameters();

            var sql = $"update {this.TableName} set ";
            var i = 0;
            foreach (var item in model.GetType().GetProperties())
            {
                var val = item.GetValue(model, null);

                if (val != null && item.Name != "Id")
                {
                    sql += $"{item.Name} = @val{i},";
                    parameter.Add($"@val{i}", val.ToString());
                    i++;
                }
            }

            sql = sql.Remove(sql.Length - 1);
            sql += " where Id = " + model.Id;

            using (var connection = GetSqlConnection())
            {
                return connection.Execute(sql, parameter) > 0;
                //return connection.Update(model);
            }
        }

        public async Task<bool> UpdateAsync(T model)
        {
            model.UpdatedOn = DateTime.UtcNow;
            DynamicParameters parameter = new DynamicParameters();

            var sql = $"update {this.TableName} set ";
            var i = 0;
            foreach (var item in model.GetType().GetProperties())
            {
                var val = item.GetValue(model, null);

                if (val != null && item.Name != "Id")
                {
                    sql += $"{item.Name} = @val{i},";
                    parameter.Add($"@val{i}", val.ToString());
                    i++;
                }
            }

            sql = sql.Remove(sql.Length - 1);
            sql += " where Id = " + model.Id;

            using (var connection = GetSqlConnection())
            {
                return await connection.ExecuteAsync(sql, parameter) > 0;
                //return await connection.UpdateAsync(model);
            }
        }

        public bool Delete(int id)
        {
            using (var connection = GetSqlConnection())
            {
                return connection.Delete(new T() { Id = id });
            }
        }

        [Obsolete]
        public bool Delete(Expression<Func<T, bool>> predicate)
        {
            using (var connection = GetSqlConnection())
            {
                return connection.Delete(predicate);
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            using (var connection = GetSqlConnection())
            {
                return await connection.DeleteAsync(new T() { Id = id });
            }
        }

        public int Create(T model)
        {
            model.CreatedOn = DateTime.UtcNow;
            using (var connection = GetSqlConnection())
            {
                return connection.Insert(model);
            }
        }

        public void Create(IEnumerable<T> model)
        {
            model.AsList().ForEach(t => { t.CreatedOn = DateTime.UtcNow; });

            using (var connection = GetSqlConnection())
            {
                connection.Insert(model);
            }

        }

        public async void CreateAsync(T model)
        {
            using (var connection = GetSqlConnection())
            {
                await connection.InsertAsync(model);
            }
        }

        public async void CreateAsync(IEnumerable<T> model)
        {
            using (var connection = GetSqlConnection())
            {
                await connection.InsertAsync(model);
            }
        }
    }
}
