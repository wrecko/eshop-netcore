﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Eshop.Core
{
    public class Hash
    {
        private const int HASH_SIZE = 32;
        private const int SALT_SIZE = 16;

        public static Tuple<string, string> HashPassword(string s, string salt = "")
        {
            byte[] saltByte;

            if (String.IsNullOrEmpty(salt))
                saltByte = GenerateSalt();
            else
                saltByte = Convert.FromBase64String(salt);

            var pbkdf2 = new Rfc2898DeriveBytes(s, saltByte, 10000, HashAlgorithmName.SHA256);

            byte[] pbkdf2Hash = pbkdf2.GetBytes(HASH_SIZE);
            byte[] result = new byte[SALT_SIZE + HASH_SIZE];

            Array.Copy(pbkdf2Hash, 0, result, 0, HASH_SIZE);
            Array.Copy(saltByte, 0, result, HASH_SIZE, SALT_SIZE);

            return new Tuple<string, string>(Convert.ToBase64String(result), Convert.ToBase64String(saltByte));
        }

        private static byte[] GenerateSalt()
        {
            byte[] salt;
            RandomNumberGenerator.Create().GetBytes(salt = new byte[SALT_SIZE]);
            return salt;
        }
    }
}
