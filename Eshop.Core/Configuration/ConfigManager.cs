﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Core
{
    public static class ConfigManager
    {
        private static IConfiguration _config { get; set; }

        public static void Initialize(IConfiguration config)
        {
            _config = config;
        }

        public static IConfiguration GetSection(string sectionName) => _config.GetSection(sectionName);

        public static string Get(string value) => _config.GetValue<string>(value);

        public static string GetConnectionString(string value) => _config.GetConnectionString(value);

    }
}
