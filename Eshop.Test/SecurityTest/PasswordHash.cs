﻿using Eshop.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Test.SecurityTest
{
    [TestClass]
    public class PasswordHash
    {
        [TestMethod]
        public void GeneratePassword_WO_Salt_Success()
        {
            var hash = Hash.HashPassword("asdasdasd");
            Assert.IsTrue(hash.Item1.Length > 0);
            Assert.IsTrue(hash.Item2.Length > 0);
        }

        [TestMethod]
        public void GeneratePassword_W_Salt_Success()
        {
            //Hash.HashPassword("asdasdasd", "123456");
        }
    }
}
