﻿using Eshop.Test.Helper;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Eshop.Core;

namespace Eshop.Test.ConfigurationTest
{
    [TestClass]
    public class JsonConfigurationTest
    {
        
        [TestInitialize]
        public void Init()
        {
            ConfigManager.Initialize(ConfigHelper.GetIConfiguration());
        }            

        [TestMethod]
        public void GetJsonConfig_Success()
        {
            Assert.IsNotNull(ConfigManager.Get("TestNotEncryptedString"));
        }

        [TestMethod]
        public void GetCollection_Success()
        {
            var section = ConfigManager.GetSection("Collection");
            var tt = section.GetValue<string>("Test1");
            Assert.IsNotNull(section);
            Assert.AreEqual(section.GetValue<string>("Test1"), "asd");
            Assert.AreEqual(section.GetValue<int>("Test2"), "123");
        }

        [TestMethod]
        public void GetDatabaseConnectionString_Success()
        {
            Assert.IsNotNull(ConfigManager.GetConnectionString("DatabaseConnectionString"));
        }
    }
}
