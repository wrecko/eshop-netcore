﻿using Eshop.Core;
using Eshop.Core.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Test.AuthTest
{
    [TestClass]
    public class AuthTest : BaseTestClass
    {
        [TestMethod, IntegrationTestCategory]
        public void Register_User_Success()
        {
            var up = new UserProvider();

            var password = Hash.HashPassword("admin");

            var user = new User()
            {
                Login = "Admin",
                Password = password.Item1,
                Salt = password.Item2,
                Role = (int)UserRoleEnum.ADMIN
            };

            up.Create(user);
        }

        [TestMethod, IntegrationTestCategory]
        public void Authorize_User_Success()
        {
            var up = new UserProvider();
            var user = up.GetSingle("login", "Admin");

            var password = Hash.HashPassword("admin", user.Salt);

            Assert.IsTrue(password.Item1 == user.Password);

        }
    }
}
