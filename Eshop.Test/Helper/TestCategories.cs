﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Test
{
    public class IntegrationTestCategoryAttribute : TestCategoryBaseAttribute
    {
        public IntegrationTestCategoryAttribute() { }

        public override IList<string> TestCategories
        {
            get { return new List<string> { "Integration Tests" }; }
        }
    }
}
