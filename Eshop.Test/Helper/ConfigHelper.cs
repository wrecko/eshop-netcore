﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Test.Helper
{
    public static class ConfigHelper
    {
        public static IConfiguration GetIConfiguration(string outputPath = "\\")
        {
            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true)                                
                .Build();
        }
    }
}
