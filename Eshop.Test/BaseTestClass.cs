﻿using Eshop.Core;
using Eshop.Test.Helper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Test
{
    [TestClass]
    public abstract class BaseTestClass
    {
        [TestInitialize]
        public void Init()
        {
            ConfigManager.Initialize(ConfigHelper.GetIConfiguration());
        }
    }
}
