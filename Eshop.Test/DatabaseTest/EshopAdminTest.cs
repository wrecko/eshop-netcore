﻿using Eshop.Core.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Test.DatabaseTest
{
    [TestClass]
    public class EshopAdminTest : BaseTestClass
    {
        [TestMethod, IntegrationTestCategory]
        public void Eshop_Category_Create_Success()
        {
            var catProvider = new CategoryProvider();

            catProvider.Create(new Category()
            {
                Name = "TestCategory"                
            });
        }

        [TestMethod, IntegrationTestCategory]
        public void Eshop_Category_Update_Success()
        {
            var catProvider = new CategoryProvider();

            catProvider.Update(new Category()
            {
                Id = 1,
                Name = "Capkaaa",
                ParentCategoryId = 0,
                UpdatedOn = DateTime.UtcNow
            });
        }
    }
}
