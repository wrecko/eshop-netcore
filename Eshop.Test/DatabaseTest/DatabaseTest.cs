﻿using Eshop.Core;
using Eshop.Core.Database;
using Eshop.Test.Helper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eshop.Test.DatabaseTest
{
    [TestClass]
    public class DatabaseTest : BaseTestClass
    {

        private UserProvider ResolveProvider() => new UserProvider();

        [TestMethod]
        public void DB_GetSingle_Success()
        {
            var up = ResolveProvider();
            var result = up.GetSingle(1);

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Login.Trim(), "Admin");
        }

        [TestMethod]
        public void DB_GetCollection_WithParameter_Success()
        {
            var up = ResolveProvider();
            var result = up.GetCollection("login", "test") as IList<User>;
            
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count > 0);
            
        }

        [TestMethod]
        public void DB_GetCollection_Success()
        {
            var up = ResolveProvider();
            var result = up.GetCollection();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count > 0);

        }

        [TestMethod]
        public void DB_Create_Success()
        {
            var up = ResolveProvider();
            var result = up.Create(new User()
            {
                Login = "Test",
                Password = "Test",
                Salt = "asdad"
            });

            Assert.IsTrue(result > 0);
        }
        

        [TestMethod]
        public void DB_Mass_Create_Success()
        {
            var up = ResolveProvider();
            up.Create(new List<User>() {
                new User()
                {
                    Login = "Test1",
                    Password = "Test1",
                    Salt = "asdad1"
                },
                new User()
                {
                    Login = "Test2",
                    Password = "Test2",
                    Salt = "asdad2"
                },
                new User()
                {
                    Login = "Test3",
                    Password = "Test3",
                    Salt = "asdad3"
                }
            });
            
        }

        [TestMethod]
        public void DB_Delete_Single_Success()
        {
            var up = ResolveProvider();
            var entities = up.GetCollection();
            var result = up.Delete(entities[entities.Count-1].Id);

            Assert.IsTrue(result);
        }

        //[TestMethod]
        //public void DB_Delete_Predicate_Success()
        //{
        //    var up = ResolveProvider();
        //    var result = up.Delete(t => t.Login == "Test");

        //    Assert.IsTrue(result);
        //}

        [TestMethod]
        public void DB_Update_Single_Success()
        {
            var up = ResolveProvider();
            var entities = up.GetCollection();
            var ent = entities[entities.Count - 1];
            var entId = ent.Id;

            var newSalt = "Traalalalalala";

            ent.Salt = newSalt;

            var result = up.Update(ent);
            ent = up.GetSingle(entId);

            Assert.IsTrue(result);
            Assert.AreEqual(newSalt, ent.Salt);
        }

    }
}
